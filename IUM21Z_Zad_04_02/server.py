from flask import Flask, request, json
import pandas as pd
import datetime
import calendar

import torch
import torch.nn as nn
from sklearn.linear_model import RidgeClassifier
import dill as pickle

from pandas.core.frame import DataFrame

app = Flask(__name__)

ab_dict = {}
curr_model_flag = True # True = "TRAINED", False = "LINEAR"

class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()  
        self.add_module('layer1', nn.Linear(32, 50))
        self.add_module('layer2', nn.Linear(50, 10))
        self.add_module('layer3', nn.Linear(10, 2))
        self.load_state_dict(torch.load('../ium/models/fourth_last', map_location="cpu"))

    def forward(self, input):
        features = self.layer1(input)
        features = self.layer2(features)
        features = self.layer3(features)
        return features

network = Net()

with open('../ium/models/clf_tree2', 'rb') as file:
    clf = pickle.load(file)

def prepare_input_data(data) -> DataFrame:
    
    discount = data["offered_discount"]
    city = data["city"]
    category_path = data["category_path"]
    price = data["price"]
    added_to_cart = data["added_to_cart"]
    timestamp = data["timestamp"]
    
    products_df = pd.read_json('../ium/data/raw/products.jsonl', lines=True)
    products_df = products_df[['price']]
    
    timestamp = timestamp.split("_")
    timestamp = timestamp[0].split("-")
    timestamp = datetime.datetime(int(timestamp[0]), int(timestamp[1]), int(timestamp[2])).weekday()
    timestamp = calendar.day_name[timestamp]
    
    d = {'added_to_cart': [added_to_cart], 'price': [price], 'Słuchawki': [0], 'Gry Xbox 360': [0], 'Monitory LCD': [0], 'Okulary 3D': [0],
         'Anteny RTV': [0], 'Gry komputerowe': [0], 'Telefony komórkowe': [0], 'Odtwarzacze DVD': [0], 'Odtwarzacze mp3 i mp4': [0],
         'Telefony stacjonarne': [0], 'Gry PlayStation3': [0], 'Biurowe urządzenia wielofunkcyjne': [0], 'Tablety': [0],
         'Zestawy głośnomówiące': [0], 'Zestawy słuchawkowe': [0], 'offered_discount': [discount], 'Warszawa': [0], 'Wrocław': [0],
         'Poznań': [0], 'Szczecin': [0], 'Kraków': [0], 'Radom': [0], 'Gdynia': [0], 'Monday': [0], 'Saturday': [0], 'Tuesday': [0],
         'Wednesday': [0], 'Friday': [0], 'Thursday': [0], 'Sunday': [0]}
    df = pd.DataFrame(data=d)
    
    df['added_to_cart'] = df['added_to_cart'].astype(float)
    df['price'] /= products_df['price'].max()
    df[category_path] = 1
    df['offered_discount'] = df['offered_discount'].astype(float)
    df[city] = 1
    df[timestamp] = 1
    
    return df

def get_prediction(data: dict) -> dict:
    
    global curr_model_flag
    in_data = prepare_input_data(data)
    
    if data["model"] == "AB":
        if data["session_id"] not in ab_dict:
            ab_dict[data["session_id"]] = curr_model_flag
            curr_model_flag = not curr_model_flag
            
        if ab_dict[data["session_id"]]:
            in_data = torch.tensor(in_data.values)
            in_data = in_data.type(torch.float32)
            output = network(in_data)
            out_dict = {"Pewność wyniku": float(output[0][torch.max(output,1)[1]]), 
                        "Czy skończy się kupnem": "Tak" if not torch.max(output,1)[1] else "Nie", 
                        "model": "TRAINED",
                        "session_id": data["session_id"]}
        else:
            output = clf.predict(in_data)
            output = output[0]
            out_dict = {"Pewność wyniku": 1, 
                        "Czy skończy się kupnem": "Tak" if int(output[0]) > 0.5 else "Nie", 
                        "model": "LINEAR",
                        "session_id": data["session_id"]}
        
    elif data["model"] == "LINEAR":       
        ab_dict.clear()
        output = clf.predict(in_data)
        output = output[0]
        out_dict = {"Pewność wyniku": 1, 
                    "Czy skończy się kupnem": "Tak" if int(output[0]) > 0.5 else "Nie", 
                    "model": data["model"],
                    "session_id": data["session_id"]}
    
    else:
        ab_dict.clear()
        in_data = torch.tensor(in_data.values)
        in_data = in_data.type(torch.float32)
        output = network(in_data)
        out_dict = {"Pewność wyniku": float(output[0][torch.max(output,1)[1]]), 
                    "Czy skończy się kupnem": "Tak" if not torch.max(output,1)[1] else "Nie", 
                    "model": data["model"],
                    "session_id": data["session_id"]}
    
    return out_dict


@app.route('/', methods=["GET"])
def process_request():
    if request.method == "GET":
        if request.get_json(force=True):
            print("Got request: ", request.json)
            response = get_prediction(request.json)
            return json.jsonify(response)
        else:
            return "Missing message"


if __name__ == '__main__':
    app.run()