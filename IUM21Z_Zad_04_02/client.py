
from flask import Flask
import requests
from argparse import ArgumentParser

app = Flask(__name__)



def main():
    args = parse_arguments()
    data = {
        "offered_discount": args.discount,
        "city": args.city,
        "category_path": args.category,
        "price": args.price,
        "added_to_cart": args.added_to_cart,
        "timestamp": args.time,
        "model": args.model,
        "session_id": args.session_id
    }

    print("Sending request:\n", data)
    response = requests.get("http://localhost:5000/", json=data)
    print("Response:")
    print(response.json())


def parse_arguments():
    p = ArgumentParser("Client for sending requests to prediction server")
    p.add_argument('-d',
                   '--discount',
                   help="Offered discount for session",
                   type=int,
                   default=0)
    p.add_argument('-c',
                   '--city',
                   help="Target city for delivery.",
                   type=str,
                   default="Warszawa",
                   choices=["Warszawa", "Wrocław", "Poznań", "Szczecin", "Kraków","Radom", "Gdynia"])
    p.add_argument('-cp',
                   '--category',
                   help="Category of the product",
                   type=str,
                   default="Słuchawki",
                   choices=["Słuchawki", "Gry Xbox 360", "Monitory LCD", "Okulary 3D", "Anteny RTV", "Gry komputerowe",
                            "Telefony komórkowe", "Odtwarzacze DVD", "Odtwarzacze mp3 i mp4", "Telefony stacjonarne",
                            "Gry PlayStation3", "Biurowe urządzenia wielofunkcyjne", "Tablety", "Zestawy głośnomówiące",
                            "Zestawy słuchawkowe"])
    p.add_argument('-p',
                   '--price',
                   help="Price of the product",
                   type=int,
                   default=10)
    p.add_argument('-ac',
                   '--added_to_cart',
                   help="Is product added to cart",
                   type=bool,
                   default=False)
    p.add_argument('-t',
                   '--time',
                   help="Time of session",
                   type=str,
                   default="2021-08-02_21:55:54")
    p.add_argument('-m',
                   '--model',
                   help="Is prediction determined by linear model",
                   type=str,
                   default="AB",
                   choices=["LINEAR", "TRAINED"])
    p.add_argument('-id',
                   '--session_id',
                   help="Id of session",
                   type=int,
                   default=1)
    return p.parse_args()

if __name__ == '__main__':
    main()